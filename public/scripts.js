document.getElementById('generate-json').addEventListener('click', function () {
    let stretchObjects = [];
    const fabricID = document.getElementById('fabricID').value;
    const description = document.getElementById('description').value;
    const type = document.getElementById('type').value;
    const weight = parseFloat(document.getElementById('weight').value);
    const thickness = parseFloat(document.getElementById('thickness').value);
    const overhangWeft = parseFloat(document.getElementById('overhangWeft').value);
    const overhangWarp = parseFloat(document.getElementById('overhangWarp').value);
    const stretchObjectsDiv = document.getElementById('stretch-objects');
    const stretchObjectCount = stretchObjectsDiv.childElementCount;
    let stretchObject = {
        lengthToForceWeft: "",
        fabricForceWeft: "",
        lengthToForceWarp: "",
        fabricForceWarp: "",
        lengthToForceBias: "",
        fabricForceBias: ""
    };
    for(let i=0; i<stretchObjectCount; i++){
      stretchObject.lengthToForceWeft = parseFloat(document.getElementById(`lengthToForceWeft-${i+1}`).value);
      stretchObject.fabricForceWeft = parseFloat(document.getElementById(`fabricForceWeft-${i+1}`).value);
      stretchObject.lengthToForceWarp = parseFloat(document.getElementById(`lengthToForceWarp-${i+1}`).value);
      stretchObject.fabricForceWarp = parseFloat(document.getElementById(`fabricForceWarp-${i+1}`).value);
      stretchObject.lengthToForceBias = parseFloat(document.getElementById(`lengthToForceBias-${i+1}`).value);
      stretchObject.fabricForceBias = parseFloat(document.getElementById(`fabricForceBias-${i+1}`).value);    
      stretchObjects.push(stretchObject);
    }

    const fabricData = {
        fabricID,
        description,
        type,
        weight,
        thickness,
        overhangWeft,
        overhangWarp,
        stretch: stretchObjects
    };
    document.getElementById('output').textContent = JSON.stringify(fabricData, null, 2);
});

document.getElementById('add-stretch-object').addEventListener('click', function () {
    const stretchObjectsDiv = document.getElementById('stretch-objects');
    const stretchObjectCount = stretchObjectsDiv.childElementCount;
    const newStretchObjectDiv = document.createElement('div');
    newStretchObjectDiv.id = `stretch-object-${stretchObjectCount + 1}`;
    newStretchObjectDiv.innerHTML = `
          <h4>Stretch Measurement ${stretchObjectCount + 1}</h4>
            
          <label for="lengthToForceWeft-${stretchObjectCount + 1}">Length of measured material Weft (cm):</label>
          <input type="number" id="lengthToForceWeft-${stretchObjectCount + 1}" name="lengthToForceWeft" required><br>

          <label for="fabricForceWeft-${stretchObjectCount + 1}">Length to Force measured Weft (cm/Newtons):</label>
          <input type="number" id="fabricForceWeft-${stretchObjectCount + 1}" name="fabricForceWeft" required><br>

          <label for="lengthToForceWarp-${stretchObjectCount + 1}">Length of measured material Warp (cm):</label>
          <input type="number" id="lengthToForceWarp-${stretchObjectCount + 1}" name="lengthToForceWarp" required><br>

          <label for="fabricForceWarp-${stretchObjectCount + 1}">Length to Force measured Warp (cm/Newtons):</label>
          <input type="number" id="fabricForceWarp-${stretchObjectCount + 1}" name="fabricForceWarp" required><br>

          <label for="lengthToForceBias-${stretchObjectCount + 1}">Length of measured material Bias (cm):</label>
          <input type="number" id="lengthToForceBias-${stretchObjectCount + 1}" name="lengthToForceBias" required><br>

          <label for="fabricForceBias-${stretchObjectCount + 1}">Length to Force measured Bias (cm/Newtons):</label>
          <input type="number" id="fabricForceBias-${stretchObjectCount + 1}" name="fabricForceBias" required><br>
      `;
      stretchObjectsDiv.appendChild(newStretchObjectDiv);
    
});


function parseCSV(fileContent) {
  const lines = fileContent.split('\n');
  const headers = lines[0].split(',');
  const values = lines[1].split(',');

  let data = {};
  for (let i = 0; i < headers.length; i++) {
    data[headers[i]] = values[i];
  }
  return data;
}

document.getElementById('csv-file').addEventListener('change', function (event) {
  const file = event.target.files[0];
  if (file) {
    const reader = new FileReader();
    reader.onload = function (e) {
      const data = parseCSV(e.target.result);
      populateForm(data);
    };
    reader.readAsText(file);
  }
});

function populateForm(data) {
  document.getElementById('fabricID').value = data['Fabric Name'];
  document.getElementById('type').value = data['Type'];
  document.getElementById('size').value = parseFloat(data['Size(mm)']);
  document.getElementById('thickness').value = parseFloat(data['Thickness(mm)']);
  document.getElementById('mass').value = parseFloat(data['Mass(g)']);
  document.getElementById('overhangWeft').value = parseFloat(data['Contact Distance Weft(mm)']);
  document.getElementById('overhangWarp').value = parseFloat(data['Contact Distance Warp(mm)']);

  // Assuming that the number of stretch length and force measurements is the same for weft, warp, and bias
  for (let i = 0; i < 5; i++) {
    document.getElementById(`stretchLengthWeft${i + 1}`).value = parseFloat(data[`Stretch Length Weft(mm)`]);
    document.getElementById(`stretchForceWeft${i + 1}`).value = parseFloat(data[`Stretch Force Weft(Kgf)`]);
    document.getElementById(`stretchLengthWarp${i + 1}`).value = parseFloat(data[`Stretch Length Warp(mm)`]);
    document.getElementById(`stretchForceWarp${i + 1}`).value = parseFloat(data[`Stretch Force Warp(Kgf)`]);
    document.getElementById(`stretchLengthBias${i + 1}`).value = parseFloat(data[`Stretch Length Bias(mm)`]);
    document.getElementById(`stretchForceBias${i + 1}`).value = parseFloat(data[`Stretch Force Bias(Kgf)`]);
  }
}
